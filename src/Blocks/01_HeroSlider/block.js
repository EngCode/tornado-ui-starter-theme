//======> Block Edit <======//
const HeroSlider_Edit = props => {
    const {setAttributes, attributes} = props;
    //=====>  Set Default Attributes <=====//
    props.setAttributes({ align: 'full' });

    //=====> JS Assets <=====//
    function jsAssets () {
        if (window.Tornado) {
            //====> Hero Slider <====//
            Tornado.slider('.hero-slider:not(.tns-slider)', {
                items      : 1,     //===> Display Slides Count
                duration   : 8000,  //===> Autoplay Duration
                pagination : true,   //===> Show Dots Pagination
                controls   : true,   //===> Show Arrows
                animation  : "gallery",    //===> Animation Mode [carousel, gallery]
                speed      : 1000,           //===> Animation Speed
            });
            //====> Dynamic Backgrounds <====//
            Tornado.setBackground('[data-src]',false);
            //=====> Prevent Links from Interactive <=====//
            Tornado.getElements('.wp-block a[href]:not(.plfi-done)').forEach(element => {
                element.addEventListener('click', event => event.preventDefault());
                element.classList.add('plfi-done');
            });
        } else return null;
    }

    //=====> Render Block Layouts <=====//
    return (
        <Panel>
            <PanelBody>
                <ServerSideRender block="tornado/hero-slider" />
                {attributes.align === 'full' && jsAssets()}
            </PanelBody>
        </Panel>
    )
};

//========> Create Block <==<======//
registerBlockType ('tornado/hero-slider', {
    title: 'Hero Slider',
    description: '- - -',
    icon:'editor-insertmore',
    category:'tornado-theme',
    keywords: [ 'ui', 'tornado', 'blocks' ],
    supports: {
        align: ['full','wide']
    },
    attributes : {},
    edit : HeroSlider_Edit,
    save : props => {return null},
});