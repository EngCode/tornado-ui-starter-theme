<?php
    //===== Services Loop Block =====//
    function hero_slider_block( $attributes, $content ) {
        //====== Top Markup =========//
        $markup = '';
        ob_start();
?>

<!-- Hero Slider -->
<?php 
    //==== Query Dynamic Options ====//
    global $wp_query;
    /*==== Grap Query Data =====*/
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'main-slider',
        'posts_per_page' => 6,
        'paged' => $paged,
        'order' => 'DESC',
    );
    $the_query = new WP_Query( $args );
    //==== Start Query =====//
    if ($the_query->have_posts() ) :
        echo '<div class="hero-slider">';
        //==== Loop Start ====//
        while ($the_query->have_posts() ): $the_query->the_post();
            //=== Block  Design ===//
            get_template_part('inc/template-parts/components/slide','block');  
        //==== End Loop =====//
        endwhile;
        echo '</div>';
        //=== Pagination ===//
        if (function_exists("pagination")) { pagination($the_query); };
        wp_reset_postdata();
        //==== if have no Posts ====//
    else : echo '<div class="alert info">' . pll__( 'Sorry no Posts have been found here.' ) . '</div>';
    //==== End Query =====//
    endif;
?>
<!-- // Hero Slider -->

<?php
        $blockOutput = ob_get_clean();
        $markup  .= $blockOutput;
        return "{$markup}";
    }
    
    register_block_type('tornado/hero-slider', array('render_callback' => 'hero_slider_block',));
?>