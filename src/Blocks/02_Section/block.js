//======> Attirbutes Data <======//
const Section_Properties = {
    //===> Custom Class <===//
    custom_class : {
        type: 'string',
        default: '',
    },
    //===> Custom ID <===//
    block_id : {
        type: 'string',
        default: '',
    },
    //===> Background <===//
    background : {
        type: 'string',
        default: null,
    },
    //===> Size <===//
    section_size : {
        type: 'string',
        default: '',
    },
    //===> Type <===//
    section_type : {
        type: 'string',
        default: 'loop',
    },
    //===> Container <===//
    section_container : {
        type: 'string',
        default: 'container',
    },
    //===> Title <===//
    section_title_op : {
        type: 'boolean',
        default: false,
    },
    section_title : {
        type: 'string',
        source: 'html',
        selector: '.section-title',
        default: '- - -',
    },
    section_title_2 : {
        type: 'string',
        default: '- - -',
    },
    //===> Grid <===//
    section_grid_op : {
        type: 'boolean',
        default: false,
    },
    grid_gutter : {
        type: 'string',
        default: '',
    },
    grid_flow : {
        type: 'string',
        default: '',
    },
    grid_align_x : {
        type: 'string',
        default: '',
    },
    grid_align_y : {
        type: 'string',
        default: '',
    },
    //===> Media Column <===//
    media_op : {
        type: 'boolean',
        default: false,
    },
    media_type : {
        type: 'string',
        default: 'image',
    },
    media_xs : {
        type: 'number',
        default: 12,
    },
    media_md : {
        type: 'number',
        default: 5,
    },
    media_lg : {
        type: 'number',
        default: 6,
    },
    media_xl : {
        type: 'number',
        default: 6,
    },
    media_file : {
        type: 'string',
        default: '',
    },
    media_frame : {
        type: 'html',
        default: '',
    },
    media_size : {
        type: 'string',
        default: '',
    },
    media_responsive : {
        type: 'boolean',
        default: true,
    },
    //===> Content Column <===//
    content_op : {
        type: 'boolean',
        default: false,
    },
    content_xs : {
        type: 'number',
        default: 12,
    },
    content_md : {
        type: 'number',
        default: 5,
    },
    content_lg : {
        type: 'number',
        default: 6,
    },
    content_xl : {
        type: 'number',
        default: 6,
    },
    //===> Dynamic Options <===//
    posts_number : {
        type: 'number',
        default: 3,
    },
    post_type : {
        type: 'string',
        default: 'post',
    },
    template_block : {
        type: 'string',
        default: 'blogs/blog-block',
    },
    //===> Dynamic Carousel <===//
    carousel_mode : {
        type: 'boolean',
        default: false,
    },
    carousel_controls : {
        type: 'boolean',
        default: false,
    },
    carousel_nav : {
        type: 'boolean',
        default: false,
    },
    carousel_xs : {
        type: 'number',
        default: 12,
    },
    carousel_sm : {
        type: 'number',
        default: 12,
    },
    carousel_md : {
        type: 'number',
        default: 5,
    },
    carousel_lg : {
        type: 'number',
        default: 6,
    },
    carousel_xl : {
        type: 'number',
        default: 6,
    },
}

//======> Block Edit <======//
const Section_Edit = props => {
    const {setAttributes, attributes} = props;
    //=======> Define Properties <======//
    props.setAttributes({ align: 'full' });
    //====> Custom ID , Class <====//
    const set_block_id = block_id => setAttributes({ block_id });
    const set_custom_class = custom_class => setAttributes({ custom_class });
    //====> Background <====//
    const set_background = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Background</button>)};
    //====> Section Settings <====//
    const set_section_size = section_size => setAttributes({ section_size });
    const set_section_type = section_type => setAttributes({ section_type });
    const set_section_container= section_container => setAttributes({ section_container });
    //====> Section Title <====//
    const set_section_title_op = section_title_op => setAttributes({ section_title_op });
    const set_section_title = section_title => setAttributes({ section_title });
    const set_section_title_2 = section_title_2 => setAttributes({ section_title_2 });
    //====> Section Grid <====//
    const set_section_grid_op = section_grid_op => setAttributes({ section_grid_op });
    const set_grid_gutter  = grid_gutter => setAttributes({ grid_gutter });
    const set_grid_flow    = grid_flow => setAttributes({ grid_flow });
    const set_grid_align_x = grid_align_x => setAttributes({ grid_align_x });
    const set_grid_align_y = grid_align_y => setAttributes({ grid_align_y });
    //====> Media Column <====//
    const set_media_op = media_op => setAttributes({ media_op });
    const set_media_type = media_type => setAttributes({ media_type });
    const set_media_xs = media_xs => setAttributes({ media_xs });
    const set_media_md = media_md => setAttributes({ media_md });
    const set_media_lg = media_lg => setAttributes({ media_lg });
    const set_media_xl = media_xl => setAttributes({ media_xl });
    const set_media_frame = media_frame => setAttributes({ media_frame });
    const set_media_file = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Media</button>)};
    const set_media_size = media_size => setAttributes({ media_size });
    const set_media_responsive = media_responsive => setAttributes({ media_responsive });
    //====> Content Column <====//
    const set_content_op = content_op => setAttributes({ content_op });
    const set_content_xs = content_xs => setAttributes({ content_xs });
    const set_content_md = content_md => setAttributes({ content_md });
    const set_content_lg = content_lg => setAttributes({ content_lg });
    const set_content_xl = content_xl => setAttributes({ content_xl });
    //====> Dynamic Options <====//
    const set_posts_number = posts_number => setAttributes({ posts_number });
    const set_post_type = post_type => setAttributes({ post_type });
    const set_template_block = template_block => setAttributes({ template_block });
    //====> Carousel Mode <====//
    const set_carousel_mode = carousel_mode => setAttributes({ carousel_mode });
    const set_carousel_controls = carousel_controls => setAttributes({ carousel_controls });
    const set_carousel_nav = carousel_nav => setAttributes({ carousel_nav });
    const set_carousel_xs = carousel_xs => setAttributes({ carousel_xs });
    const set_carousel_sm = carousel_sm => setAttributes({ carousel_sm });
    const set_carousel_md = carousel_md => setAttributes({ carousel_md });
    const set_carousel_lg = carousel_lg => setAttributes({ carousel_lg });
    const set_carousel_xl = carousel_xl => setAttributes({ carousel_xl });

    //=======> JS Assets <======//
    function jsAssets () {
        if (window.Tornado) {
            //======> Dynamic Backgrounds <======//
            Tornado.setBackground('[data-src]',false);
            //=======> Prevent Links from Interactive <======//
            Tornado.getElements('.wp-block a[href]:not(.plfi-done)').forEach(element => {
                element.addEventListener('click', event => event.preventDefault());
                element.classList.add('plfi-done');
            });
            //====> Carousel Slider <====//
            Tornado.getElements('.carousel-slider:not(.tns-slider)').forEach(element => {
                //====> Dynamic Options <====//
                var itemsNumber = element.getAttribute('data-items'),
                    itemsSmall  = element.getAttribute('data-sm'),
                    itemsMedium = element.getAttribute('data-md'),
                    itemsLarge  = element.getAttribute('data-lg'),
                    itemsxLarge  = element.getAttribute('data-xl'),
                    controls    = element.getAttribute('data-controls'),
                    cssClass    = element.getAttribute('data-class') || 'carousel-slider-outer',
                    pagination  = element.getAttribute('data-pagination');

                //====> Create Slider <====//
                Tornado.slider(element, {
                    duration   : 7000,
                    speed      : 700,
                    items      : parseInt(itemsNumber) || 1,
                    pagination : pagination ? true : false,
                    controls   : controls ? true : false,
                    uniuqClass : cssClass,
                    responsive : {
                        small  : { items: parseInt(itemsSmall)  || 2},
                        medium : { items: parseInt(itemsMedium) || 3},
                        large  : { items: parseInt(itemsLarge)  || 4},
                        xlarge : { items: parseInt(itemsxLarge)  || 5},
                    }
                });
            });
        } else return null;
    }

    //=======> Render Block Layouts <======//
    return (
        <Panel>
            {/* //====> Controls Layout <====// */}
            <InspectorControls key="inspector">
                {/*=====> Controls Panel <=====*/}
                <PanelBody title="Block Settings">
                    {/*=== Custom ID ===*/}
                    <TextControl key="block_id" label="CSS ID" value={ attributes.block_id } onChange={ set_block_id } />
                    {/*=== Custom Class ===*/}
                    <TextControl key="custom_class" label="CSS Class" value={ attributes.custom_class } onChange={ set_custom_class } />
                    {/*=== Section Size ===*/}
                    <SelectControl key="section_size" label="Section Size" value={ attributes.section_size } onChange={set_section_size} options={[
                        { label: 'None',   value: '' },
                        { label: 'Small',  value: 'section-small' },
                        { label: 'Medium', value: 'section-medium' },
                        { label: 'Large',  value: 'section-large' },
                    ]}/>
                    {/*=== Section Type ===*/}
                    <SelectControl key="section_type" label="Section Type" value={ attributes.section_type } onChange={set_section_type} options={[
                        { label: 'Dynamic Loop',  value: 'loop' },
                        { label: 'Hero Content',  value: 'hero' },
                    ]}/>
                    {/*=== Section Container ===*/}
                    <SelectControl key="section_container" label="Section Container" value={ attributes.section_container } onChange={set_section_container} options={[
                        { label: 'Standard', value: 'container' },
                        { label: 'Large',    value: 'container-xl' },
                        { label: 'Fluid',    value: 'container-fluid' },
                    ]}/>
                    {/*=== Background ===*/}
                    <MediaUpload render={({open}) => set_background(open)} value={ attributes.background } onSelect={media => { setAttributes({background: media.url}); }} type="image" />
                </PanelBody>
                {/*=====> Controls Panel <=====*/}
                <PanelBody title="Block Features">
                    {/*=== Section Title ===*/}
                    <ToggleControl label="Section Title" checked={ attributes.section_title_op } onChange={ set_section_title_op } />
                    {/*=== Section Grid ===*/}
                    {attributes.section_type == 'hero' &&
                        <ToggleControl label="Section Grid" checked={ attributes.section_grid_op } onChange={ set_section_grid_op } />
                    }
                </PanelBody>
                {/*=====> Controls Panel <=====*/}
                {attributes.section_type == 'loop' &&
                    <PanelBody title="Dynamic Options">
                        {/*=== Section Title ===*/}
                        {attributes.section_title_op == true && 
                            <TextControl key="section_title" label="Section Title" value={ attributes.section_title_2 } onChange={ set_section_title_2 } />
                        }
                        {/*=== Post Types ===*/}
                        <SelectControl key="post_type" label="Post-Type" value={ attributes.post_type } onChange={set_post_type} options={[
                            { label: 'Blog',     value: 'post' },
                            { label: 'Services', value: 'services' },
                            { label: 'Testimonials', value: 'testimonials' },
                            { label: 'Partners', value: 'partners' },
                        ]}/>
                        {/*=== Design Template ===*/}
                        <TextControl key="template_block" label="Template Path" value={ attributes.template_block } onChange={ set_template_block } />
                        {/*=== Posts Number ===*/}
                        <RangeControl label="Posts Number" value={ attributes.posts_number } onChange={ set_posts_number } min={1} max={24}/>
                        {/*=== Carousel Mode ===*/}
                        <ToggleControl label="Carousel Mode" checked={ attributes.carousel_mode } onChange={ set_carousel_mode } />
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.carousel_mode == true &&
                    <PanelBody title="Carousel Options">
                        {/*=== Carousel Control ===*/}
                        <ToggleControl label="Carousel Controls" checked={ attributes.carousel_controls } onChange={ set_carousel_controls } />
                        {/*=== Carousel Nav ===*/}
                        <ToggleControl label="Carousel Pagination" checked={ attributes.carousel_nav } onChange={ set_carousel_nav } />
                        {/*=== Columns ===*/}
                        <RangeControl label="Items in XS" value={ attributes.carousel_xs } onChange={ set_carousel_xs } min={1} max={12}/>
                        <RangeControl label="Items in SM" value={ attributes.carousel_sm } onChange={ set_carousel_sm } min={1} max={12}/>
                        <RangeControl label="Items in MD" value={ attributes.carousel_md } onChange={ set_carousel_md } min={1} max={12}/>
                        <RangeControl label="Items in LG" value={ attributes.carousel_lg } onChange={ set_carousel_lg } min={1} max={12}/>
                        <RangeControl label="Items in XL" value={ attributes.carousel_xl } onChange={ set_carousel_xl } min={1} max={12}/>
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.section_grid_op == true &&
                    <PanelBody title="Grid Options">
                        {/*=== Media Column ===*/}
                        <ToggleControl label="Media Column" checked={ attributes.media_op } onChange={ set_media_op } />
                        {/*=== Content Column ===*/}
                        <ToggleControl label="Content Column" checked={ attributes.content_op } onChange={ set_content_op } />
                        {/*=== Grid Gutter ===*/}
                        <SelectControl key="grid_gutter" label="Grid Gutter" value={ attributes.grid_gutter } onChange={set_grid_gutter} options={[
                            { label: 'Standard',   value: '' },
                            { label: 'None',   value: 'no-gutter' },
                            { label: 'Small',  value: 'gutter-small' },
                            { label: 'Medium', value: 'gutter-medium' },
                            { label: 'Large',  value: 'gutter-large' },
                        ]}/>
                        {/*=== Grid Align ===*/}
                        <SelectControl key="grid_flow" label="Grid Flow" value={ attributes.grid_flow } onChange={set_grid_flow} options={[
                            { label: 'Normal',  value: '' },
                            { label: 'Reverse', value: 'row-reverse' },
                        ]}/>
                        {/*=== Grid Align X ===*/}
                        <SelectControl key="grid_align_x" label="Grid Align-X" value={ attributes.grid_align_x } onChange={set_grid_align_x} options={[
                            { label: 'None',    value: '' },
                            { label: 'Start',   value: 'align-start-x' },
                            { label: 'Center',  value: 'align-center-x' },
                            { label: 'Between', value: 'align-between' },
                            { label: 'Around',  value: 'align-around' },
                            { label: 'End',     value: 'align-end-x' },
                        ]}/>
                        {/*=== Grid Align Y ===*/}
                        <SelectControl key="grid_align_y" label="Grid Align-Y" value={ attributes.grid_align_y } onChange={set_grid_align_y} options={[
                            { label: 'None',    value: '' },
                            { label: 'Start',   value: 'align-start-y' },
                            { label: 'Center',  value: 'align-center-y' },
                            { label: 'End',     value: 'align-end-y' },
                        ]}/>
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.media_op == true &&
                    <PanelBody title="Media Options">
                        {/*=== Media Type ===*/}
                        <SelectControl key="media_type" label="Media Type" value={ attributes.media_type } onChange={set_media_type} options={[
                            { label: 'Image',  value: 'image' },
                            { label: 'Video',  value: 'video' },
                            { label: 'Iframe', value: 'iframe' },
                        ]}/>
                        {/*=== Media Type ===*/}
                        <ToggleControl label="Responsive Media" checked={ attributes.media_responsive } onChange={ set_media_responsive } />
                        {/*=== Media File ===*/}
                        { attributes.media_type !== 'iframe' && 
                            <MediaUpload render={({open}) => set_media_file(open)} value={ attributes.media_file } onSelect={media => { setAttributes({media_file : media.url}); }} type={ attributes.media_type } />
                        } { attributes.media_type == 'iframe' && 
                            <TextareaControl label="Iframe" help="Enter iframe Embed Code" value={ attributes.media_frame } onChange={ set_media_frame } />
                        }
                        {/*=== Media Size ===*/}
                        { attributes.media_responsive == true } {
                            <SelectControl key="media_size" label="Media Size" value={ attributes.media_size } onChange={set_media_size} options={[
                                { label: 'Standard',  value: '' },
                                { label: 'HD Size',  value: 'hd-size' },
                                { label: 'Square Size', value: 'square-size' },
                            ]}/>
                        }
                        {/*=== Media Sizes ===*/}
                        <RangeControl label="Column Size XS" value={ attributes.media_xs } onChange={ set_media_xs } min={1} max={12}/>
                        <RangeControl label="Column Size MD" value={ attributes.media_md } onChange={ set_media_md } min={1} max={12}/>
                        <RangeControl label="Column Size LG" value={ attributes.media_lg } onChange={ set_media_lg } min={1} max={12}/>
                        <RangeControl label="Column Size XL" value={ attributes.media_xl } onChange={ set_media_xl } min={1} max={12}/>
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.content_op == true &&
                    <PanelBody title="Content Options">
                        {/*=== Content Size ===*/}
                        <RangeControl label="Column Size XS" value={ attributes.content_xs } onChange={ set_content_xs } min={1} max={12}/>
                        <RangeControl label="Column Size MD" value={ attributes.content_md } onChange={ set_content_md } min={1} max={12}/>
                        <RangeControl label="Column Size LG" value={ attributes.content_lg } onChange={ set_content_lg } min={1} max={12}/>
                        <RangeControl label="Column Size XL" value={ attributes.content_xl } onChange={ set_content_xl } min={1} max={12}/>
                    </PanelBody>
                }
                {/*=====> End Controls Panel <=====*/}
            </InspectorControls>
            {/* //====> Block Layout <====// */}
            <PanelBody>
                {/*=====> Dynamic Loop <=====*/}
                {attributes.section_type == 'loop' &&
                    <ServerSideRender block="tornado/tornado-section" attributes={{
                        section_title_2: attributes.section_title_2,
                        posts_number: attributes.posts_number,
                        custom_class: attributes.custom_class,
                        block_id: attributes.block_id,
                        section_size: attributes.section_size,
                        background: attributes.background,
                        section_container: attributes.section_container,
                        section_title_op: attributes.section_title_op,
                        post_type: attributes.post_type,
                        template_block: attributes.template_block,
                        carousel_mode: attributes.carousel_mode,
                        carousel_controls: attributes.carousel_controls,
                        carousel_nav: attributes.carousel_nav,
                        carousel_xs: attributes.carousel_xs,
                        carousel_md: attributes.carousel_md,
                        carousel_lg: attributes.carousel_lg,
                        carousel_xl: attributes.carousel_xl,
                    }}/>
                }

                {/*=====> Hero Content <=====*/}
                {attributes.section_type == 'hero' &&
                    <div className={`hero-section ${attributes.section_size} ${attributes.custom_class}`} data-src={attributes.background}>
                        {/*=====> Container <=====*/}
                        <div className={attributes.section_container}>
                            {/*====> Section Title <====*/}
                            {attributes.section_title_op == true && 
                                <RichText tagName="h2" className='section-title' onChange={ set_section_title } value={ attributes.section_title } placeholder="- - -" />
                            }
                            {/*====> Grid <====*/}
                            {attributes.section_grid_op == true &&
                                <div className={`row ${attributes.grid_gutter} ${attributes.grid_flow} ${attributes.grid_align_x} ${attributes.grid_align_y}`}>
                                    {/*====> Media Column <====*/}
                                    {attributes.media_op == true &&
                                        <div className={`media col-${attributes.media_xs} col-m-${attributes.media_md} col-l-${attributes.media_lg} col-xl-${attributes.media_xl}`}>
                                            {/*===> Image <===*/}
                                            {attributes.media_type == 'image' && <>
                                                {attributes.media_responsive == false &&
                                                    <img src={attributes.media_file} alt="media image" />
                                                } {attributes.media_responsive == true && 
                                                    <div className={`responsive-element ${attributes.media_size}`} data-src={attributes.media_file}></div>
                                                }
                                            </>}
                                            {/*===> Video <===*/}
                                            {attributes.media_type == 'video' && 
                                                <div className={`responsive-element ${attributes.media_size}`} >
                                                    <video controls><source src={attributes.media_file} type="video/mp4" /></video>
                                                </div>
                                            }
                                            {/*===> iFrame <===*/}
                                            {attributes.media_type == 'iframe' && 
                                                <div className={`responsive-element ${attributes.media_size}`} >
                                                    {`${attributes.media_frame}`}
                                                </div>
                                            }
                                        </div>
                                    }
                                    {/*====> Content Column <====*/}
                                    {attributes.content_op == true &&
                                        <div className={`content col-${attributes.content_xs} col-m-${attributes.content_md} col-l-${attributes.content_lg} col-xl-${attributes.content_xl}`}>
                                            <InnerBlocks />
                                        </div>
                                    }
                                    {/*====> End Content Column <====*/}
                                </div>
                            }
                            {/*====> None Grid <====*/}
                            {attributes.section_grid_op == false && <InnerBlocks />}
                        </div>
                        {/*=====> End Container <=====*/}
                    </div>
                }

                {/*=====> Assets <=====*/}
                {attributes.align === 'full' && jsAssets()}
            </PanelBody>
        </Panel>
    );
};

//======> Block Save <======//
const Section_Save = props => {
    const {setAttributes, attributes} = props;
    //=======> Condtional Render <======//
    const condRend = () => {
        {/*=====> Hero Content <=====*/}
        if (attributes.section_type === 'hero') {
            <div className={`hero-section ${attributes.section_size} ${attributes.custom_class}`} data-src={attributes.background}>
                {/*=====> Container <=====*/}
                <div className={attributes.section_container}>
                    {/*====> Section Title <====*/}
                    {attributes.section_title_op == true && <h2 className="section-title">{attributes.section_title}</h2>}
                    {/*====> Grid <====*/}
                    {attributes.section_grid_op == true &&
                        <div className={`row ${attributes.grid_gutter} ${attributes.grid_flow} ${attributes.grid_align_x} ${attributes.grid_align_y}`}>
                            {/*====> Media Column <====*/}
                            {attributes.media_op == true &&
                                <div className={`media col-${attributes.media_xs} col-m-${attributes.media_md} col-l-${attributes.media_lg} col-xl-${attributes.media_xl}`}>
                                    {/*===> Image <===*/}
                                    {attributes.media_type == 'image' && <>
                                        {attributes.media_responsive == false &&
                                            <img src={attributes.media_file} alt="media image" />
                                        } {attributes.media_responsive == true && 
                                            <div className={`responsive-element ${attributes.media_size}`} data-src={attributes.media_file}></div>
                                        }
                                    </>}
                                    {/*===> Video <===*/}
                                    {attributes.media_type == 'video' && 
                                        <div className={`responsive-element ${attributes.media_size}`} >
                                            <video controls><source src={attributes.media_file} type="video/mp4" /></video>
                                        </div>
                                    }
                                    {/*===> iFrame <===*/}
                                    {attributes.media_type == 'iframe' && 
                                        <div className={`responsive-element ${attributes.media_size}`} >
                                            {`${attributes.media_frame}`}
                                        </div>
                                    }
                                </div>
                            }
                            {/*====> Content Column <====*/}
                            {attributes.content_op == true &&
                                <div className={`content col-${attributes.content_xs} col-m-${attributes.content_md} col-l-${attributes.content_lg} col-xl-${attributes.content_xl}`}>
                                    <InnerBlocks.Content />
                                </div>
                            }
                            {/*====> End Content Column <====*/}
                        </div>
                    }
                    {/*====> None Grid <====*/}
                    {attributes.section_grid_op == false && <InnerBlocks.Content />}
                </div>
                {/*=====> End Container <=====*/}
            </div>
        } else return null;
    }
    //=======> Render Block Layouts <======//
    return condRend();
};

//========> Create Block <==<======//
registerBlockType ('tornado/tornado-section', {
    title: 'Tornado Section',
    description: '- - -',
    icon:'editor-insertmore',
    category:'tornado-theme',
    keywords: [ 'ui', 'tornado', 'blocks' ],
    supports: {
        align: ['full','wide']
    },
    attributes : Section_Properties,
    edit : Section_Edit,
    save : Section_Save,
});

