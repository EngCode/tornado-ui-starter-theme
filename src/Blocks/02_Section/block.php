
<?php
    //===== Services Loop Block =====//
    function tornado_section_block( $attributes, $content ) {
        //====== Top Markup =========//
        $markup = '';
        ob_start();
?>

<div class="<?php echo $attributes['section_size'] .' '. $attributes['custom_class'] ;?>" <?php echo 'data-src="'.$attributes['background'].'"'; ?>>
    <!-- Container -->
    <div class=<?php echo $attributes['section_container']; ?>>
        <!-- Section Title -->
        <?php if ($attributes['section_title_op'] == true) : ?>
            <h2 class="section-title"><?php echo $attributes['section_title_2']; ?></h2>
        <?php endif; ?>
        <!-- Grid -->
        <div class="row last-row <?php if ($attributes['carousel_mode'] == true) : echo 'carousel-slider'; endif;?>"
            <?php if ($attributes['carousel_xs'] == true) : echo 'data-items="'.$attributes['carousel_xs'].'"'; endif;?>
            <?php if ($attributes['carousel_sm'] == true) : echo 'data-sm="'.$attributes['carousel_sm'].'"'; endif;?>
            <?php if ($attributes['carousel_md'] == true) : echo 'data-md="'.$attributes['carousel_md'].'"'; endif;?>
            <?php if ($attributes['carousel_lg'] == true) : echo 'data-lg="'.$attributes['carousel_lg'].'"'; endif;?>
            <?php if ($attributes['carousel_xl'] == true) : echo 'data-xl="'.$attributes['carousel_xl'].'"'; endif;?>
            <?php if ($attributes['carousel_controls'] == true) : echo 'data-controls="'.$attributes['carousel_controls'].'"'; endif;?>
            <?php if ($attributes['carousel_nav'] == true) : echo 'data-pagination="'.$attributes['carousel_nav'].'"'; endif;?>
        >
            <?php 
                //==== Query Dynamic Options ====//
                global $wp_query;
                /*==== Grap Query Data =====*/
                $args = array(
                    'post_type' => $attributes['post_type'],
                    'posts_per_page' => $attributes['posts_number'],
                );
                $the_query = new WP_Query( $args );
                //==== Start Query =====//
                if ($the_query->have_posts() ) :
                    //==== Loop Start ====//
                    while ($the_query->have_posts() ): $the_query->the_post();
                        //=== Block  Design ===//
                        get_template_part('inc/template-parts/'.$attributes['template_block']);  
                    //==== End Loop =====//
                    endwhile;
                    wp_reset_postdata();
                //==== End Query =====//
                endif;
            ?>
        </div>
        <!-- // Grid -->
    </div>
    <!-- // Container -->
</div>

<?php
        $blockOutput = ob_get_clean();
        $markup  .= $blockOutput;
        return "{$markup}";
    }
    
    register_block_type('tornado/tornado-section', array(
        'attributes' => array(
            'section_title_2' => array(
                'type' => 'string',
                'default' =>  '- - -',
            ),

            'block_id' => array(
                'type' => 'string',
                'default' =>  '',
            ),

            'custom_class' => array(
                'type' => 'string',
                'default' =>  '',
            ),
            
            'posts_number' => array(
                'type' => 'number',
                'default' =>  '3',
            ),

            'section_size' => array(
                'type' => 'string',
                'default' =>  '',
            ),

            'background' => array(
                'type' => 'string',
                'default' =>  '',
            ),

            'section_container' => array(
                'type' => 'string',
                'default' =>  'container',
            ),

            'section_title_op' => array(
                'type' => 'boolean',
                'default' =>  false,
            ),

            'post_type' => array(
                'type' => 'string',
                'default' =>  'post',
            ),

            'template_block' => array(
                'type' => 'string',
                'default' =>  'blogs/blog-block',
            ),

            'carousel_mode' => array(
                "type" =>  'boolean',
                "default" =>  false,
            ),

            'carousel_controls' => array(
                "type" =>  'boolean',
                "default" =>  false,
            ),

            'carousel_nav' => array(
                "type" =>  'boolean',
                "default" =>  false,
            ),

            'carousel_xs' => array(
                "type" =>  'number',
                "default" =>  12,
            ),

            'carousel_sm' => array(
                "type" =>  'number',
                "default" =>  12,
            ),

            'carousel_md' => array(
                "type" =>  'number',
                "default" =>  5,
            ),

            'carousel_lg' => array(
                "type" =>  'number',
                "default" =>  6,
            ),

            'carousel_xl' => array(
                "type" =>  'number',
                "default" =>  6,
            ),
        ),

        'render_callback' => 'tornado_section_block',
    ));
?>