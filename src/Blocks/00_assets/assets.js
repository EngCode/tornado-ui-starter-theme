//======> GB Modules <======//
const { registerBlockType } = wp.blocks;

/*===== Gutenberg Form Controls =====*/
const {
    Draggable,
    Panel,
    PanelBody,
    PanelRow,
    SelectControl,
    Button,
    DropdownMenu,
    withAPIData,
    TextControl,
    RangeControl,
    FormFileUpload,
    BaseControl,
    ToggleControl,
    CheckboxControl,
    TextareaControl
} = wp.components;

/*===== Gutenberg Rich Control =====*/
const {
    PlainText,
    RichText,
    Editable,
    MediaUpload,
    MediaUploadCheck,
    AlignmentToolbar,
    BlockControls,
    ColorPalette,
    InnerBlocks,
    InspectorControls
} = wp.editor;

/*===== Editor Server Render =====*/
const { serverSideRender : ServerSideRender } = wp;
