<?php
    //==== Custom Block Categories ====//
    add_filter( 'block_categories', 'tornado_categories', 10, 2);

    function tornado_categories( $categories, $post ) {
        return array_merge( $categories, array(
            //==== Tornado =====//
            array(
                'slug' => 'tornado',
                'title' => __( 'Tornado Blocks', 'tornado' ),
            ),
        ));
    }

    //==== Calling Blocks Example =====//
    require dirname(__FILE__) . '/01_HeroSlider/block.php';
    require dirname(__FILE__) . '/02_Section/block.php';
?>