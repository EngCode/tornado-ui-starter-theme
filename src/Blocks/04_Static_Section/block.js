//======> Attirbutes Data <======//
const SS_Properties = {
    //===> Custom Class <===//
    custom_class : {
        type: 'string',
        default: '',
    },
    //===> Custom ID <===//
    block_id : {
        type: 'string',
        default: '',
    },
    //===> Background <===//
    background : {
        type: 'string',
        default: null,
    },
    //===> Size <===//
    SS_size : {
        type: 'string',
        default: '',
    },
    //===> Type <===//
    SS_type : {
        type: 'string',
        default: 'hero',
    },
    //===> Container <===//
    SS_container : {
        type: 'string',
        default: 'container',
    },
    //===> Title <===//
    SS_title_op : {
        type: 'boolean',
        default: false,
    },
    SS_title : {
        type: 'string',
        source: 'html',
        selector: '.section-title',
        default: '- - -',
    },
    //===> Grid <===//
    SS_grid_op : {
        type: 'boolean',
        default: false,
    },
    grid_gutter : {
        type: 'string',
        default: '',
    },
    grid_flow : {
        type: 'string',
        default: '',
    },
    grid_align_x : {
        type: 'string',
        default: '',
    },
    grid_align_y : {
        type: 'string',
        default: '',
    },
    //===> Media Column <===//
    media_op : {
        type: 'boolean',
        default: false,
    },
    media_type : {
        type: 'string',
        default: 'image',
    },
    media_xs : {
        type: 'number',
        default: 12,
    },
    media_md : {
        type: 'number',
        default: 5,
    },
    media_lg : {
        type: 'number',
        default: 6,
    },
    media_xl : {
        type: 'number',
        default: 6,
    },
    media_file : {
        type: 'string',
        default: '',
    },
    media_frame : {
        type: 'html',
        default: '',
    },
    media_size : {
        type: 'string',
        default: '',
    },
    media_responsive : {
        type: 'boolean',
        default: true,
    },
    //===> Content Column <===//
    content_op : {
        type: 'boolean',
        default: false,
    },
    content_xs : {
        type: 'number',
        default: 12,
    },
    content_md : {
        type: 'number',
        default: 5,
    },
    content_lg : {
        type: 'number',
        default: 6,
    },
    content_xl : {
        type: 'number',
        default: 6,
    },
}

//======> Block Edit <======//
const SS_Edit = props => {
    const {setAttributes, attributes} = props;
    //=======> Define Properties <======//
    props.setAttributes({ align: 'full' });
    //====> Custom ID , Class <====//
    const set_block_id = block_id => setAttributes({ block_id });
    const set_custom_class = custom_class => setAttributes({ custom_class });
    //====> Background <====//
    const set_background = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Background</button>)};
    //====> Section Settings <====//
    const set_SS_size = SS_size => setAttributes({ SS_size });
    const set_SS_type = SS_type => setAttributes({ SS_type });
    const set_SS_container= SS_container => setAttributes({ SS_container });
    //====> Section Title <====//
    const set_SS_title_op = SS_title_op => setAttributes({ SS_title_op });
    const set_SS_title = SS_title => setAttributes({ SS_title });
    //====> Section Grid <====//
    const set_SS_grid_op = SS_grid_op => setAttributes({ SS_grid_op });
    const set_grid_gutter  = grid_gutter => setAttributes({ grid_gutter });
    const set_grid_flow    = grid_flow => setAttributes({ grid_flow });
    const set_grid_align_x = grid_align_x => setAttributes({ grid_align_x });
    const set_grid_align_y = grid_align_y => setAttributes({ grid_align_y });
    //====> Media Column <====//
    const set_media_op = media_op => setAttributes({ media_op });
    const set_media_type = media_type => setAttributes({ media_type });
    const set_media_xs = media_xs => setAttributes({ media_xs });
    const set_media_md = media_md => setAttributes({ media_md });
    const set_media_lg = media_lg => setAttributes({ media_lg });
    const set_media_xl = media_xl => setAttributes({ media_xl });
    const set_media_frame = media_frame => setAttributes({ media_frame });
    const set_media_file = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Media</button>)};
    const set_media_size = media_size => setAttributes({ media_size });
    const set_media_responsive = media_responsive => setAttributes({ media_responsive });
    //====> Content Column <====//
    const set_content_op = content_op => setAttributes({ content_op });
    const set_content_xs = content_xs => setAttributes({ content_xs });
    const set_content_md = content_md => setAttributes({ content_md });
    const set_content_lg = content_lg => setAttributes({ content_lg });
    const set_content_xl = content_xl => setAttributes({ content_xl });

    //=======> JS Assets <======//
    function jsAssets () {
        if (window.Tornado) {
            //======> Dynamic Backgrounds <======//
            Tornado.setBackground('[data-src]',false);
            //=======> Prevent Links from Interactive <======//
            Tornado.getElements('.wp-block a[href]:not(.plfi-done)').forEach(element => {
                element.addEventListener('click', event => event.preventDefault());
                element.classList.add('plfi-done');
            });
            //====> Carousel Slider <====//
            Tornado.getElements('.carousel-slider:not(.tns-slider)').forEach(element => {
                //====> Dynamic Options <====//
                var itemsNumber = element.getAttribute('data-items'),
                    itemsSmall  = element.getAttribute('data-sm'),
                    itemsMedium = element.getAttribute('data-md'),
                    itemsLarge  = element.getAttribute('data-lg'),
                    itemsxLarge  = element.getAttribute('data-xl'),
                    controls    = element.getAttribute('data-controls'),
                    cssClass    = element.getAttribute('data-class') || 'carousel-slider-outer',
                    pagination  = element.getAttribute('data-pagination');

                //====> Create Slider <====//
                Tornado.slider(element, {
                    duration   : 7000,
                    speed      : 700,
                    items      : parseInt(itemsNumber) || 1,
                    pagination : pagination ? true : false,
                    controls   : controls ? true : false,
                    uniuqClass : cssClass,
                    responsive : {
                        small  : { items: parseInt(itemsSmall)  || 2},
                        medium : { items: parseInt(itemsMedium) || 3},
                        large  : { items: parseInt(itemsLarge)  || 4},
                        xlarge : { items: parseInt(itemsxLarge)  || 5},
                    }
                });
            });
        } else return null;
    }

    //=======> Render Block Layouts <======//
    return (
        <Panel>
            {/* //====> Controls Layout <====// */}
            <InspectorControls key="inspector">
                {/*=====> Controls Panel <=====*/}
                <PanelBody title="Block Settings">
                    {/*=== Custom ID ===*/}
                    <TextControl key="block_id" label="CSS ID" value={ attributes.block_id } onChange={ set_block_id } />
                    {/*=== Custom Class ===*/}
                    <TextControl key="custom_class" label="CSS Class" value={ attributes.custom_class } onChange={ set_custom_class } />
                    {/*=== Section Size ===*/}
                    <SelectControl key="SS_size" label="Section Size" value={ attributes.SS_size } onChange={set_SS_size} options={[
                        { label: 'None',   value: '' },
                        { label: 'Small',  value: 'section-small' },
                        { label: 'Medium', value: 'section-medium' },
                        { label: 'Large',  value: 'section-large' },
                    ]}/>
                    {/*=== Section Type ===*/}
                    <SelectControl key="SS_type" label="Section Type" value={ attributes.SS_type } onChange={set_SS_type} options={[
                        // { label: 'Dynamic Loop',  value: 'loop' },
                        { label: 'Hero Content',  value: 'hero' },
                    ]}/>
                    {/*=== Section Container ===*/}
                    <SelectControl key="SS_container" label="Section Container" value={ attributes.SS_container } onChange={set_SS_container} options={[
                        { label: 'Standard', value: 'container' },
                        { label: 'Large',    value: 'container-xl' },
                        { label: 'Fluid',    value: 'container-fluid' },
                    ]}/>
                    {/*=== Background ===*/}
                    <MediaUpload render={({open}) => set_background(open)} value={ attributes.background } onSelect={media => { setAttributes({background: media.url}); }} type="image" />
                </PanelBody>
                {/*=====> Controls Panel <=====*/}
                <PanelBody title="Block Features">
                    {/*=== Section Title ===*/}
                    <ToggleControl label="Section Title" checked={ attributes.SS_title_op } onChange={ set_SS_title_op } />
                    {/*=== Section Grid ===*/}
                    {attributes.SS_type == 'hero' &&
                        <ToggleControl label="Section Grid" checked={ attributes.SS_grid_op } onChange={ set_SS_grid_op } />
                    }
                </PanelBody>
                {/*=====> Controls Panel <=====*/}
                {attributes.SS_grid_op == true &&
                    <PanelBody title="Grid Options">
                        {/*=== Media Column ===*/}
                        <ToggleControl label="Media Column" checked={ attributes.media_op } onChange={ set_media_op } />
                        {/*=== Content Column ===*/}
                        <ToggleControl label="Content Column" checked={ attributes.content_op } onChange={ set_content_op } />
                        {/*=== Grid Gutter ===*/}
                        <SelectControl key="grid_gutter" label="Grid Gutter" value={ attributes.grid_gutter } onChange={set_grid_gutter} options={[
                            { label: 'Standard',   value: '' },
                            { label: 'None',   value: 'no-gutter' },
                            { label: 'Small',  value: 'gutter-small' },
                            { label: 'Medium', value: 'gutter-medium' },
                            { label: 'Large',  value: 'gutter-large' },
                        ]}/>
                        {/*=== Grid Align ===*/}
                        <SelectControl key="grid_flow" label="Grid Flow" value={ attributes.grid_flow } onChange={set_grid_flow} options={[
                            { label: 'Normal',  value: '' },
                            { label: 'Reverse', value: 'row-reverse' },
                        ]}/>
                        {/*=== Grid Align X ===*/}
                        <SelectControl key="grid_align_x" label="Grid Align-X" value={ attributes.grid_align_x } onChange={set_grid_align_x} options={[
                            { label: 'None',    value: '' },
                            { label: 'Start',   value: 'align-start-x' },
                            { label: 'Center',  value: 'align-center-x' },
                            { label: 'Between', value: 'align-between' },
                            { label: 'Around',  value: 'align-around' },
                            { label: 'End',     value: 'align-end-x' },
                        ]}/>
                        {/*=== Grid Align Y ===*/}
                        <SelectControl key="grid_align_y" label="Grid Align-Y" value={ attributes.grid_align_y } onChange={set_grid_align_y} options={[
                            { label: 'None',    value: '' },
                            { label: 'Start',   value: 'align-start-y' },
                            { label: 'Center',  value: 'align-center-y' },
                            { label: 'End',     value: 'align-end-y' },
                        ]}/>
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.media_op == true &&
                    <PanelBody title="Media Options">
                        {/*=== Media Type ===*/}
                        <SelectControl key="media_type" label="Media Type" value={ attributes.media_type } onChange={set_media_type} options={[
                            { label: 'Image',  value: 'image' },
                            { label: 'Video',  value: 'video' },
                            { label: 'Iframe', value: 'iframe' },
                        ]}/>
                        {/*=== Media Type ===*/}
                        <ToggleControl label="Responsive Media" checked={ attributes.media_responsive } onChange={ set_media_responsive } />
                        {/*=== Media File ===*/}
                        { attributes.media_type !== 'iframe' && 
                            <MediaUpload render={({open}) => set_media_file(open)} value={ attributes.media_file } onSelect={media => { setAttributes({media_file : media.url}); }} type={ attributes.media_type } />
                        } { attributes.media_type == 'iframe' && 
                            <TextareaControl label="Iframe" help="Enter iframe Embed Code" value={ attributes.media_frame } onChange={ set_media_frame } />
                        }
                        {/*=== Media Size ===*/}
                        { attributes.media_responsive == true } {
                            <SelectControl key="media_size" label="Media Size" value={ attributes.media_size } onChange={set_media_size} options={[
                                { label: 'Standard',  value: '' },
                                { label: 'HD Size',  value: 'hd-size' },
                                { label: 'Square Size', value: 'square-size' },
                            ]}/>
                        }
                        {/*=== Media Sizes ===*/}
                        <RangeControl label="Column Size XS" value={ attributes.media_xs } onChange={ set_media_xs } min={1} max={12}/>
                        <RangeControl label="Column Size MD" value={ attributes.media_md } onChange={ set_media_md } min={1} max={12}/>
                        <RangeControl label="Column Size LG" value={ attributes.media_lg } onChange={ set_media_lg } min={1} max={12}/>
                        <RangeControl label="Column Size XL" value={ attributes.media_xl } onChange={ set_media_xl } min={1} max={12}/>
                    </PanelBody>
                }
                {/*=====> Controls Panel <=====*/}
                {attributes.content_op == true &&
                    <PanelBody title="Content Options">
                        {/*=== Content Size ===*/}
                        <RangeControl label="Column Size XS" value={ attributes.content_xs } onChange={ set_content_xs } min={1} max={12}/>
                        <RangeControl label="Column Size MD" value={ attributes.content_md } onChange={ set_content_md } min={1} max={12}/>
                        <RangeControl label="Column Size LG" value={ attributes.content_lg } onChange={ set_content_lg } min={1} max={12}/>
                        <RangeControl label="Column Size XL" value={ attributes.content_xl } onChange={ set_content_xl } min={1} max={12}/>
                    </PanelBody>
                }
                {/*=====> End Controls Panel <=====*/}
            </InspectorControls>
            {/* //====> Block Layout <====// */}
            <PanelBody>
                {/*=====> Hero Content <=====*/}
                {attributes.SS_type == 'hero' &&
                    <div className={`hero-section ${attributes.SS_size} ${attributes.custom_class}`} data-src={attributes.background}>
                        {/*=====> Container <=====*/}
                        <div className={attributes.SS_container}>
                            {/*====> Section Title <====*/}
                            {attributes.SS_title_op == true && 
                                <RichText tagName="h2" className='section-title' onChange={ set_SS_title } value={ attributes.SS_title } placeholder="- - -" />
                            }
                            {/*====> Grid <====*/}
                            {attributes.SS_grid_op == true &&
                                <div className={`row ${attributes.grid_gutter} ${attributes.grid_flow} ${attributes.grid_align_x} ${attributes.grid_align_y}`}>
                                    {/*====> Media Column <====*/}
                                    {attributes.media_op == true &&
                                        <div className={`media col-${attributes.media_xs} col-m-${attributes.media_md} col-l-${attributes.media_lg} col-xl-${attributes.media_xl}`}>
                                            {/*===> Image <===*/}
                                            {attributes.media_type == 'image' && <>
                                                {attributes.media_responsive == false &&
                                                    <img src={attributes.media_file} alt="media image" />
                                                } {attributes.media_responsive == true && 
                                                    <div className={`responsive-element ${attributes.media_size}`} data-src={attributes.media_file}></div>
                                                }
                                            </>}
                                            {/*===> Video <===*/}
                                            {attributes.media_type == 'video' && 
                                                <div className={`responsive-element ${attributes.media_size}`} >
                                                    <video controls><source src={attributes.media_file} type="video/mp4" /></video>
                                                </div>
                                            }
                                            {/*===> iFrame <===*/}
                                            {attributes.media_type == 'iframe' && 
                                                <div className={`responsive-element ${attributes.media_size}`} >
                                                    {`${attributes.media_frame}`}
                                                </div>
                                            }
                                        </div>
                                    }
                                    {/*====> Content Column <====*/}
                                    {attributes.content_op == true &&
                                        <div className={`content col-${attributes.content_xs} col-m-${attributes.content_md} col-l-${attributes.content_lg} col-xl-${attributes.content_xl}`}>
                                            <InnerBlocks />
                                        </div>
                                    }
                                    {/*====> End Content Column <====*/}
                                </div>
                            }
                            {/*====> None Grid <====*/}
                            {attributes.SS_grid_op == false && <InnerBlocks />}
                        </div>
                        {/*=====> End Container <=====*/}
                    </div>
                }

                {/*=====> Assets <=====*/}
                {attributes.align === 'full' && jsAssets()}
            </PanelBody>
        </Panel>
    );
};

//======> Block Save <======//
const SS_Save = props => {
    const {setAttributes, attributes} = props;
    //=======> Render Block Layouts <======//
    return (
        <div className={`hero-section ${attributes.SS_size} ${attributes.custom_class}`} data-src={attributes.background}>
            {/*=====> Container <=====*/}
            <div className={attributes.SS_container}>
                {/*====> Section Title <====*/}
                {attributes.SS_title_op == true && <h2 className="section-title">{attributes.SS_title}</h2>}
                {/*====> Grid <====*/}
                {attributes.SS_grid_op == true &&
                    <div className={`row ${attributes.grid_gutter} ${attributes.grid_flow} ${attributes.grid_align_x} ${attributes.grid_align_y}`}>
                        {/*====> Media Column <====*/}
                        {attributes.media_op == true &&
                            <div className={`media col-${attributes.media_xs} col-m-${attributes.media_md} col-l-${attributes.media_lg} col-xl-${attributes.media_xl}`}>
                                {/*===> Image <===*/}
                                {attributes.media_type == 'image' && <>
                                    {attributes.media_responsive == false &&
                                        <img src={attributes.media_file} alt="media image" />
                                    } {attributes.media_responsive == true && 
                                        <div className={`responsive-element ${attributes.media_size}`} data-src={attributes.media_file}></div>
                                    }
                                </>}
                                {/*===> Video <===*/}
                                {attributes.media_type == 'video' && 
                                    <div className={`responsive-element ${attributes.media_size}`} >
                                        <video controls><source src={attributes.media_file} type="video/mp4" /></video>
                                    </div>
                                }
                                {/*===> iFrame <===*/}
                                {attributes.media_type == 'iframe' && 
                                    <div className={`responsive-element ${attributes.media_size}`} >
                                        {`${attributes.media_frame}`}
                                    </div>
                                }
                            </div>
                        }
                        {/*====> Content Column <====*/}
                        {attributes.content_op == true &&
                            <div className={`content col-${attributes.content_xs} col-m-${attributes.content_md} col-l-${attributes.content_lg} col-xl-${attributes.content_xl}`}>
                                <InnerBlocks.Content />
                            </div>
                        }
                        {/*====> End Content Column <====*/}
                    </div>
                }
                {/*====> None Grid <====*/}
                {attributes.SS_grid_op == false && <InnerBlocks.Content />}
            </div>
            {/*=====> End Container <=====*/}
        </div>
    );
};

//========> Create Block <==<======//
registerBlockType ('tornado/tornado-section-static', {
    title: 'Tornado Section [Hero]',
    description: '- - -',
    icon:'editor-insertmore',
    category:'tornado-theme',
    keywords: [ 'ui', 'tornado', 'blocks' ],
    supports: {
        align: ['full','wide']
    },
    attributes : SS_Properties,
    edit : SS_Edit,
    save : SS_Save,
});

