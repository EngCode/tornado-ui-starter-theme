//======> Attirbutes Data <======//
const About_Properties = {
    //===> Custom Class <===//
    custom_class : {
        type: 'string',
        default: '',
    },
    //===> Custom ID <===//
    block_id : {
        type: 'string',
        default: '',
    },
    //===> Background <===//
    background : {
        type: 'string',
        default: null,
    },
    //===> Title <===//
    section_title : {
        type: 'string',
        source: 'html',
        selector: '.content-title',
        default: '- - -',
    },
    //===> Paragraph <===//
    section_paragraph : {
        type: 'string',
        source: 'html',
        selector: '.content-paragraph',
        default: '- - -',
    },
    //===> Media <===//
    media_file : {
        type: 'string',
        default: '',
    },
    //===> Button <===//
    btn_url : {
        type: 'string',
        default: '',
    },
    btn_text : {
        type: 'string',
        default: '',
    },

}

//======> Block Edit <======//
const About_Edit = props => {
    const {setAttributes, attributes} = props;
    //=======> Define Properties <======//
    props.setAttributes({ align: 'full' });
    //====> Custom ID , Class <====//
    const set_block_id = block_id => setAttributes({ block_id });
    const set_custom_class = custom_class => setAttributes({ custom_class });
    //====> Background <====//
    const set_background = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Background</button>)};
    //====> Section Title <====//
    const set_section_title = section_title => setAttributes({ section_title });
    //====> Section Paragraph <====//
    const set_section_paragraph = section_paragraph => setAttributes({ section_paragraph });
    //====> Section Button <====//
    const set_btn_url = btn_url => setAttributes({ btn_url });
    const set_btn_text = btn_text => setAttributes({ btn_text });
    //====> Media <====//
    const set_media_file = (openEvent) => {return(<button onClick={ openEvent } className="components-button block-lvl mb15 is-primary">Change Media</button>)};

    //=======> JS Assets <======//
    function jsAssets () {
        if (window.Tornado) {
            //======> Dynamic Backgrounds <======//
            Tornado.setBackground('[data-src]',false);
            //=======> Prevent Links from Interactive <======//
            Tornado.getElements('.wp-block a[href]:not(.plfi-done)').forEach(element => {
                element.addEventListener('click', event => event.preventDefault());
                element.classList.add('plfi-done');
            });
        } else return null;
    }

    //=======> Render Block Layouts <======//
    return (
        <Panel>
            {/* //====> Controls Layout <====// */}
            <InspectorControls key="inspector">
                {/*=====> Controls Panel <=====*/}
                <PanelBody title="Block Settings">
                    {/*=== Custom ID ===*/}
                    <TextControl key="block_id" label="CSS ID" value={ attributes.block_id } onChange={ set_block_id } />
                    {/*=== Custom Class ===*/}
                    <TextControl key="custom_class" label="CSS Class" value={ attributes.custom_class } onChange={ set_custom_class } />
                    {/*=== Background ===*/}
                    <MediaUpload render={({open}) => set_background(open)} value={ attributes.background } onSelect={media => { setAttributes({background: media.url}); }} type="image" />
                    {/*=== Media File ===*/}
                    <MediaUpload render={({open}) => set_media_file(open)} value={ attributes.media_file } onSelect={media => { setAttributes({media_file : media.url}); }} type={ attributes.media_type } />
                    {/*=== Button Options ===*/}
                    <TextControl key="btn_url" label="Button URL" value={ attributes.btn_url } onChange={ set_btn_url } />
                    <TextControl key="btn_text" label="Button Text" value={ attributes.btn_text } onChange={ set_btn_text } />
                </PanelBody>
                {/*=====> End Controls Panel <=====*/}
            </InspectorControls>
            {/* //====> Block Layout <====// */}
            <PanelBody>
                <div className={`section-medium hero-section ${attributes.section_size} ${attributes.custom_class}`} data-src={attributes.background}>
                    <div class="container overflow">
                        <div class="row align-center-y content">
                            {/* <!-- Image --> */}
                            <div class="col-12 col-m-5 col-l-6 view-status" data-animation="fadeInStart">
                                <img src={attributes.media_file} alt="" class="round-corner" />
                            </div>
                            {/* <!-- Content --> */}
                            <div class="col-12 col-m-7 col-l-6">
                                <RichText tagName="h3" className='content-title view-status' data-animation="fadeInUp" onChange={ set_section_title } value={ attributes.section_title } placeholder="- - -" />
                                <RichText tagName="p" className='content-paragraph view-status' data-animation="fadeInUp" onChange={ set_section_paragraph } value={ attributes.section_paragraph } placeholder="- - -" />
                                {attributes.btn_text !== '' &&
                                    <a href={attributes.btn_url} class="more ti-arrow-left-c with-line view-status" data-animation="fadeInUp">{attributes.btn_text}</a>
                                }
                                </div>
                            {/* <!-- // Content --> */}
                        </div>
                    </div>
                </div>

                {/*=====> Assets <=====*/}
                {attributes.align === 'full' && jsAssets()}
            </PanelBody>
        </Panel>
    );
};

//======> Block Save <======//
const About_Save = props => {
    const {setAttributes, attributes} = props;
    //=======> Render Block Layouts <======//
    return (<>
        <div className={`section-medium hero-section  about-us white-bg ${attributes.section_size} ${attributes.custom_class}`} data-src={attributes.background}>
            <div class="container overflow">
                <div class="row align-center-y content">
                    {/* <!-- Image --> */}
                    <div class="col-12 col-m-5 col-l-6 view-status" data-animation="fadeInStart">
                        <img src={attributes.media_file} alt="" class="round-corner" />
                    </div>
                    {/* <!-- Content --> */}
                    <div class="col-12 col-m-7 col-l-6">
                        <h3 class="view-status content-title" data-animation="fadeInUp">{attributes.section_title}</h3>
                        <p class="view-status content-paragraph" data-animation="fadeInUp">{attributes.section_paragraph}</p>
                        {attributes.btn_text !== '' &&
                            <a href={attributes.btn_url} class="more ti-arrow-left-c with-line view-status" data-animation="fadeInUp">{attributes.btn_text}</a>
                        }
                    </div>
                    {/* <!-- // Content --> */}
                </div>
            </div>
        </div>
    </>);
};

//========> Create Block <==<======//
registerBlockType ('tornado/about-us', {
    title: 'About US',
    description: '- - -',
    icon:'editor-insertmore',
    category:'tornado-theme',
    keywords: [ 'ui', 'tornado', 'blocks' ],
    supports: {
        align: ['full','wide']
    },
    attributes : About_Properties,
    edit : About_Edit,
    save : About_Save,
});

