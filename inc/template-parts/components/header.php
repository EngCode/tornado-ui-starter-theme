<?php
    /**
     * Tornado Theme - Custom Header Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Main Header -->
<header class="tornado-header custom" data-sticky="absolute">
    <div class="container">
        <!-- Logo -->
        <a href="<?php echo site_url();?>" class="logo"> <img src="<?php echo get_option('theme_logo');?>" alt="<?php echo bloginfo('name');?>"> </a>
        <!-- Navigation Menu -->
        <div class="navigation-menu" data-id="main-menu">
            <?php echo wp_nav_menu(array(
                'menu' => 'main-menu',
                'theme_location' => 'main-menu',
                'container' => false,
                'container_class' => false,
            ));?>
        </div>
        <!-- Action Buttons -->
        <div class="action-btns">
            <!-- Social Media -->
            <a href="https://wa.me/<?php echo get_option('whatsapp_number');?>" class="icon-btn ti-whatsapp"></a>
            <a href="<?php echo get_option('facebook_url');?>" class="icon-btn ti-facebook"></a>
            <a href="<?php echo get_option('twitter_url');?>" class="icon-btn ti-twitter"></a>
            <a href="<?php echo get_option('instagram_url');?>" class="icon-btn ti-instagram"></a>
            <a href="<?php echo get_option('linkedin_url');?>" class="icon-btn ti-linkedin"></a>
            <a href="<?php echo get_option('youtube_url');?>" class="icon-btn ti-youtube-play hidden-s-down"></a>
            <!-- Menu Mobile Button -->
            <a href="#" class="icon-btn tooltip-bottom menu-btn ti-menu-round" title="<?php echo pll__('القائمة الرئيسية','tornado');?>" data-id="main-menu"></a>
        </div>
        <!-- // Action Buttons -->
    </div>
</header>
<!-- // Main Header -->

<!-- Floating Whatsapp -->
<a href="https://wa.me/<?php echo get_option('whatsapp_number');?>" target="_blank" class="ti-whatsapp whatsapp-bg btn large circle floating-wa"></a>

<!-- Service Form -->
<div class="modal-box" id="service-form">
    <!-- Container -->
    <div class="modal-content mxw-480">
        <!-- Headline -->
        <h2 class="modal-head">
            <?php echo pll__('نموذج طلب خدمة', 'tornado'); ?>
            <button class="close-modal ti-close"></button>
        </h2>
        <!-- Content -->
        <div class="modal-body form-ui small">
            <?php echo do_shortcode('[contact-form-7 id="243" title="Service Order"]'); ?>
        </div>
        <!-- // Content -->
    </div>
    <!--// Container -->
</div>
<!-- // Service Form -->


