<?php
/**
 * this template repersent Blog Block Design component
 * @package Tornado Wordpress
 * @subpackage Developing Starter Template
 * @since Tornado UI Starter 1.0
*/
    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Testimonials Block -->
<div class="col-12 col-m-3 box-5x1 testimonials-block">
    <div class="content-box ti-play">
        <i class="image"><img src="<?php if(has_post_thumbnail()){the_post_thumbnail_url();} ?>" alt="<?php the_title(); ?>"></i>
        <h3><?php the_title(); ?></h3>
        <p><?php the_excerpt(); ?></p>
        <audio class="hidden" preload="none">
            <source src="<?php the_field('audio-file'); ?>" type="audio/mp3">
        </audio>
    </div>
</div>
<!-- // Testimonials Block -->