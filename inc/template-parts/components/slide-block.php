<?php
    /**
     * Tornado Theme - Breadcrumb Component for Yoast SEO
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>

<!-- Item -->
<div class="item">
    <a href="<?php echo get_field('cutsom-url'); ?>" class="block-lvl responsive-element" data-src="<?php if(has_post_thumbnail()){the_post_thumbnail_url();} ?>"></a>
</div>
<!-- // Item -->