<?php
    /**
     * Tornado Theme - Blog Categories List Design Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Widget Block -->
<div class="widget-block mb30">
    <h3 class="head"><?php echo pll__('قائمة الخدمات', 'tornado') ?></h3>
    <ul class="links">
        <?php 
            //==== Query Dynamic Options ====//
            global $wp_query;
            /*==== Grap Query Data =====*/
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array(
                'post_type' => 'services',
                'posts_per_page' => 12,
            );
            $the_query = new WP_Query( $args );
            //==== Start Query =====//
            if ($the_query->have_posts() ) :
                //==== Loop Start ====//
                while ($the_query->have_posts() ): $the_query->the_post();
            ?>
            <li><a href="<?php the_permalink(); ?>" class="ti-folder-bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
            <?php
                    //==== End Loop =====//
                endwhile;
                wp_reset_postdata();
            //==== End Query =====//
            endif;
        ?>
    </ul>
</div>
<!-- // Widget Block -->