<?php
    /**
     * Tornado Theme - Custom Navigation Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
    if (get_option('top_nav') == 1) :
?>
<!-- Top Navigation -->
<div class="top-navigation">
    <div class="container flexbox align-center-y align-between">
        <?php if (get_option('top_nav_contacts') == 1) : ?>
        <!-- Informations -->
        <ul class="info-list">
            <?php if (get_option('phone_number') !== '') : ?><li class="ti-phone-classic"><a href="tel:<?php echo get_option('phone_number');?>" class="ltr"><?php echo get_option('phone_number');?></a></li><?php endif; ?>
            <?php if (get_option('contact_email') !== '') : ?><li class="ti-email-new-letter hidden-s-down"><a href="mailto:<?php echo get_option('contact_email');?>" class="ltr"><?php echo get_option('contact_email');?></a></li><?php endif; ?>
        </ul>
        <?php endif; ?>

        <?php if (get_option('top_nav_links') == 1) : ?>
        <!-- Links -->
        <ul class="links">
            <li><a href="login.html" class="ti-user"><?php echo pll__('تسجيل الدخول', 'tornado'); ?></a></li>
            <li><a href="register.html" class="ti-user-cog hidden-s-down"><?php echo pll__('انشاء حساب جديد', 'tornado'); ?></a></li>
        </ul>
        <!-- // Links -->
        <?php endif; ?>
    </div>
</div>
<!-- // Top Navigation -->
<?php endif; ?>