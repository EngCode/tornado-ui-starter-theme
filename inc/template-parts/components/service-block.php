<?php
    /**
     * Tornado Theme - Blog Block Design Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Service Block -->
<div class="col-12 col-m-6 col-l-4 service-block custom mb30">
    <div class="content-box">
        <!-- Image -->
        <a href="<?php the_permalink(); ?>" class="icon"><img src="<?php if(has_post_thumbnail()){the_post_thumbnail_url();} ?>" alt="<?php the_title(); ?>"></a>
        <!-- Content -->
        <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
        <p><?php echo strip_tags(get_the_excerpt()); ?></p>
        <!-- Button -->
        <div class="btns">
            <a href="<?php the_permalink(); ?>" class="btn small secondary"><?php echo pll__('تفاصيل الخدمة','tornado'); ?></a>
            <a href="#" class="btn small primary" data-modal="service-form"><?php echo pll__('طلب الخدمة','tornado'); ?></a>
        </div>
    </div>
</div>
<!-- // Service Block -->