<?php
/**
 * this template repersent Blog Block Design component
 * @package Tornado Wordpress
 * @subpackage Developing Starter Template
 * @since Tornado UI Starter 1.0
*/
    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Logo Block -->
<div class="col-12 col-m-3 box-5x1 logo-block">
    <div class="content-box">
        <img src="<?php if(has_post_thumbnail()){the_post_thumbnail_url();} ?>" alt="<?php the_title(); ?>">
    </div>
</div>
<!-- // Logo Block -->