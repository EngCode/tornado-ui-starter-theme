<?php
    /**
     * Tornado Theme - Custom Footer Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Main Footer -->
<footer class="main-footer darker-bg">
    <div class="container">
        <!-- Grid -->
        <div class="row align-between">
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-3 mb20">
                <h3 class="group-title"><?php echo pll__('اقسام الموقع', 'tornado'); ?></h3>
                <!-- Menu -->
                <?php echo wp_nav_menu(array(
                    'menu' => 'footer-menu',
                    'theme_location' => 'footer-menu',
                    'container' => false,
                    'container_class' => false,
                ));?>
            </div>
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-3 mb20">
                <h3 class="group-title"><?php echo pll__('خدماتنا', 'tornado'); ?></h3>
                <!-- Menu -->
                <?php echo wp_nav_menu(array(
                    'menu' => 'footer-menu-2',
                    'theme_location' => 'footer-menu-2',
                    'container' => false,
                    'container_class' => false,
                ));?>
                <!-- // Menu -->
            </div>
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-3 mb20">
                <h3 class="group-title"><?php echo pll__('مواقع صديقة', 'tornado'); ?></h3>
                <!-- Menu -->
                <?php echo wp_nav_menu(array(
                    'menu' => 'footer-menu-3',
                    'theme_location' => 'footer-menu-3',
                    'container' => false,
                    'container_class' => false,
                ));?>
                <!-- // Menu -->
            </div>
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-3 mb20">
                <h3 class="group-title"><?php echo pll__('تواصل معنا', 'tornado'); ?></h3>
                <!-- Info Item -->
                <h5 class="info-item mb10 tx-nowrap ti-phone-in-talk"><a href="tel:<?php echo get_option('phone_number');?>" class="ltr"><?php echo get_option('phone_number');?></a></h5>
                <h5 class="info-item mb10 tx-nowrap ti-whatsapp"><a href="https://wa.me/<?php echo get_option('whatsapp_number');?>" class="ltr"><?php echo get_option('whatsapp_number');?></a></h5>
                <h5 class="info-item mb10 tx-nowrap ti-mail"><a href="mailto:<?php echo get_option('contact_email');?>" class="ltr"><?php echo get_option('contact_email');?></a></h5>
                <!-- Social Buttons -->
                <div class="social-btns mt10">
                    <a href="<?php echo get_option('facebook_url');?>" class="btn primary square small ti-facebook"></a>
                    <a href="<?php echo get_option('twitter_url');?>" class="btn primary square small ti-twitter"></a>
                    <a href="<?php echo get_option('instagram_url');?>" class="btn primary square small ti-instagram"></a>
                    <a href="https://wa.me/<?php echo get_option('whatsapp_number');?>" class="btn primary square small ti-whatsapp"></a>
                </div>
                <!-- Social Buttons -->
            </div>
            <!-- Links -->
        </div>
        <!-- // Grid -->
    </div>
</footer>
<!-- // Main Footer -->

<!-- Copyrights -->
<div class="copyrights darker-bg">
    <div class="container tx-align-center">
        <p><?php echo pll__(get_option('meta_copyrights'),'tornado'); ?></p>
    </div>
</div>
<!-- // Copyrights -->