<?php
    /**
     * Tornado Theme - Breadcrumb Component for Yoast SEO
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>
<!-- Page Head -->
<div class="page-head primary-bg">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <!-- Breadcrumb -->
        <div class="breadcrumb">
            <?php the_breadcrumb(''); ?>
        </div>
        <!-- // Breadcrumb -->
    </div>
</div>
<!-- Page Head -->
