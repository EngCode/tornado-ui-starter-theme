<?php
    /**
     * Tornado Theme - Lateast Blogs Loop Component for Widgets
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
    /*==== Grap Query Data =====*/
    $args = array('post_type' => 'post','posts_per_page' => 3);
    $lateast_query = new WP_Query( $args );
?>
<div class="widget-block mb30">
    <!-- Title -->
    <h3 class="head"><?php echo pll__('أحدث المقالات', 'tornado'); ?></h3>
    <?php
        //==== Start Query =====//
        if ( $lateast_query->have_posts() ) : 
            //==== Loop Start ====//
            while ( $lateast_query->have_posts() ) : $lateast_query->the_post(); 
                //=== Get the Design Part ===//
                get_template_part('inc/template-parts/blogs/blog','widget');
            //==== End Loop =====//
            endwhile;
            //==== Grid Element Close ====//
            wp_reset_postdata();
        endif; 
        //==== End Query =====//
    ?>
</div>