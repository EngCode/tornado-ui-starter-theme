<?php
    /**
     * Tornado Theme - Blog Categories List Design Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
    //====> Get Categories <====//
    $categories = get_categories( array(
        'orderby' => 'name',
        'order'   => 'ASC'
    ));
?>
<!-- Widget Block -->
<div class="widget-block mb30">
    <h3 class="head"><?php echo pll__('أقسام المدونة', 'tornado') ?></h3>
    <ul class="links">
        <?php foreach ($categories as $category) : ?>
        <li><a href="<?php echo get_category_link($category->cat_ID); ?>" class="ti-folder-bookmark" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<!-- // Widget Block -->