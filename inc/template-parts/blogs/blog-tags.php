<?php
    /**
     * Tornado Theme - Blog Tags Links Component
     * @package Tornado Wordpress
    */

    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
?>

<!-- Tags -->
<div class="widget-block mb30">
    <h3 class="head ti-tag"><?php echo pll__('الكلمات الدلالية', 'tornado'); ?></h3>
    <?php if (the_tags()) : ?>
    <div class="tags clear-after">
        <?php the_tags(' ',' ',' '); ?>
    </div>
    <?php endif; ?>
</div>