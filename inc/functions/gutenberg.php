<?php
    //======= Exit if Try to Access Directly =======//
    defined('ABSPATH') || exit;
    if (!defined('WPINC')) {die;}

    //==== Disable Gutenberg for Custom Post Types ====//
    function disable_gutenberg($is_enabled, $post_type) {
        //===> Blog Post Type <===//
        if (get_option('blog_gutenberg') !== 1 && $post_type === 'post') return false;
        //===> Slider Post Type <===//
        if (get_option('slider_gutenberg') !== 1 && $post_type === 'main-slider') return false;
        //===> Services Post Type <===//
        if (get_option('services_gutenberg') !== 1 && $post_type === 'services') return false;
        //===> Projects Post Type <===//
        if (get_option('projects_gutenberg') !== 1 && $post_type === 'projects') return false;
        //===> Portfolio Post Type <===//
        if (get_option('portfolio_gutenberg') !== 1 && $post_type === 'portfolio') return false;
        //===> Products Post Type <===//
        if (get_option('products_gutenberg') !== 1 && $post_type === 'products') return false;
        //===> Testimonials Post Type <===//
        if (get_option('testimonials_gutenberg') !== 1 && $post_type === 'testimonials') return false;
        //===> Partners Post Type <===//
        if (get_option('partners_gutenberg') !== 1 && $post_type === 'partners') return false;
        //===> F.A.Q Post Type <===//
        if (get_option('faq_gutenberg') !== 1 && $post_type === 'faq') return false;
        //===> Multimedia Post Type <===//
        if (get_option('multimedia_gutenberg') !== 1 && $post_type === 'multimedia') return false;

        return $is_enabled;
    }

    add_filter('use_block_editor_for_post_type', 'disable_gutenberg', 10, 2);
?>