<?php
    /**
     * this template for displaying Rich Content for any of this pages single.php or page.php
     * @package Tornado Wordpress
    */
?>

<!-- Head Tag -->
<?php get_header(); ?>
<!-- Custom Header -->
<?php get_template_part('inc/template-parts/components/header'); ?>
<!-- Page Head -->
<?php get_template_part('inc/template-parts/components/breadcumb'); ?>


<!-- Page Content -->
<div class="primary-ofwhite">
    <div class="container page-content">
        <!-- Grid -->
        <div class="row row-reverse">
            <!-- Content Side -->
            <div class="col-12 col-m-8 col-l-9">
                <!-- Content Details -->
                <?php 
                    //====== if Have Post Content =======//
                    if (have_posts()) : 
                        while (have_posts()) : the_post();
                            echo get_template_part('inc/template-parts/blogs/blog-details');
                    //====== End if Have Post Content =======//
                        endwhile; 
                    endif;
                ?>
            </div>
            <!-- Widget Side -->
            <div class="col-12 col-m-4 col-l-3">
                <?php echo get_template_part('inc/template-parts/blogs/blog-categories'); ?>
                <?php echo get_template_part('inc/template-parts/components/services-list-widget'); ?>
                <?php echo get_template_part('inc/template-parts/blogs/lateast-blogs'); ?>
                <?php echo get_template_part('inc/template-parts/blogs/blog-tags'); ?>
                <!-- Dynamic Sidebar -->
                <?php if (is_active_sidebar('blog-sidebar')) : ?>
                    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
                <?php endif; ?>
            </div>
            <!-- // Widget Side -->
        </div>
        <!-- // Grid -->
    </div>
</div>
<!-- // Page Content -->

<!-- Custom Footer --> 
<?php get_template_part('inc/template-parts/components/footer'); ?>
<!-- Footer -->
<?php get_footer(); ?> 