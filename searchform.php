<?php
    /**
     * this template for displaying Search Form
     * @package Tornado Wordpress
    */
?>
<!-- Search Form -->
<div class="widget-block mb30">
    <h3 class="head ti-search"><?php echo pll__('البحث السريع','tornado'); ?></h3>
    <form class="form-ui" method="get" action="<?php echo home_url( '/' ); ?>">
		<input type="text" placeholder="<?php echo pll__('كلمات البحث','tornado'); ?>" name="s" id="search-input">
        <input type="submit" value="<?php echo pll__('بحث', 'tornado'); ?>" class="btn primary block-lvl">
    </form>
</div>