<?php
    /**
     * Template Name: Error 404
     * this template for displaying 404 pages (not found)
     * @package Tornado Wordpress
    */
?>

<!-- Head Tag -->
<?php get_header(); ?>
<!-- Header -->
<?php get_template_part('inc/template-parts/components/header'); ?>
<!-- Page Head -->
<?php get_template_part('inc/template-parts/components/breadcumb'); ?>
<!-- Page Content -->
<div class="error404page pt50 pb50 flexbox align-center-z white-bg">
    <img src="<?php echo get_template_directory_uri().'/dist/img/error/error2.jpg'; ?>" alt="error-404">
    <h1 class="danger-color block-lvl tx-align-center display-h1 tx-uppercase lineheight1">Error 404</h1>
</div>
<!-- Custom Footer --> 
<?php get_template_part('inc/template-parts/components/footer'); ?>
<!-- Footer -->
<?php get_footer(); ?> 