<?php
/**
 * this template for displaying 404 pages (not found)
 * @package Tornado Wordpress
 * @subpackage Developing Starter Template
 * @since Tornado UI Starter 1.0
 * Template Name: Success
*/
?>

<!-- Head Tag -->
<?php get_header(); ?>
<!-- Header -->
<?php get_template_part('inc/template-parts/components/header'); ?>
<!-- Page Head -->
<?php get_template_part('inc/template-parts/components/breadcumb'); ?>
<!-- Page Content -->
<div class="error404page pt50 pb50 flexbox align-center-z white-bg">
    <img src="<?php echo get_template_directory_uri().'/dist/img/error/success1.png'; ?>" alt="error-404">
    <h2 class="danger-color block-lvl tx-align-center display-h1 tx-uppercase lineheight1"><?php echo pll__('لقد تم ارسالة طلبك بنجاح', 'tornado'); ?></h2>
    <a href="<?php echo home_url();?>" class="btn secondary round-corner"><?php echo pll__('العودة للرئيسية', 'tornado'); ?></a>
</div>
<!-- Custom Footer --> 
<?php get_template_part('inc/template-parts/components/footer'); ?>
<!-- Footer -->
<?php get_footer(); ?> 
